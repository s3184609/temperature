package nl.utwente.di.bookQuote;

public class TemperatureConverter {

    public double convertCelsius(String temp) {
        double c = Double.parseDouble(temp);
        double f = (c * 9/5) + 32;
        return f;
    }
}
